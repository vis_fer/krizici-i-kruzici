if ('serviceWorker' in navigator && 'PushManager' in window) {
  navigator.serviceWorker.register('./service-worker.js');
  // console.log('Service Worker is registered');
}


Vue.component('gametitle', {
  template: '#gametitle-template'
});

Vue.component('gametable', {
  // Varijable koje primamo i mijenjamo
  props: [ "player1name", "player2name" ],
  data: function(){
    return {
      table: [
        ["", "", ""] ,
        ["", "", ""] ,
        ["", "", ""]
      ],
      player: true,
      winner: "",
    }
  },
  // Izračunate vrijednosti
  computed:{
    // Vrijednost varijable player1 je "ime 1. igrača (X)"
    player1: function () {
      return this.player1name + " (X)";
    },

    // Vrijednost varijable player2 je "ime 2. igrača (O)"
    player2: function () {
      return this.player2name + " (O)";
    }
  },
  methods:{
    // Označava polje koje se odabralo
    selectCell: function(row, cell){

      // Ne može se igrati nakon što imamo pobjednika
      if(this.winner !== "")
        return;

      // Ako je polje već označeno onda ga preskoči
      if(this.table[row][cell] !== "")
        return;

      // Označi polje s oznakom trenutnog igrača (X ili O9
      // Polja u VueJs nisu reaktivna (VueJS ne detektira dobro kad se mijenjaju elementi polja, pa ih treba
      // promijeniti "ručno" sa $set
      this.$set(this.table[row], cell, this.player?"X":"O");

      // Prebaci na drugog igrača
      // "Klasične" varijable u VueJS su reaktivne, pa ispravno detektira promjenu u njima
      this.player = !this.player;

      // Provjeri ima li pobjednika
      this.checkWin();
    },

    // Postavljanje pobjednika
    setWinner:function(){
      // Prvo smo promijenili igrača, a zatim provjeravamo tko je pobjednik.
      // Zbog toga je sada na redu "gubitnik", pa trebamo odabrati suprotnog igrača kao "pobjednika"
      if(this.player){
        this.winner = this.player2;
        this.$emit("increasescore", "player2");
        // this.player2score+=1;
      }else{
        this.winner = this.player1;
        this.$emit("increasescore", "player1");
        // this.player1score+=1;
      }
    },

    // Provjera je li igrač pobjedio ili je nerješeno
    checkWin:function(){
      let i, j;

      // Ovo se sigurno može napisati i kraće :) (ali ovako je više školski)
      for(i=0; i<3; i++){
        // Tri ista u stupcu
        if(
            this.table[i][0] !== "" // Ne smiju biti sva tri prazna
            && this.table[i][0] === this.table[i][1]
            && this.table[i][0] === this.table[i][2]
        ){
          this.setWinner();
          return;
        }

        // Tri ista u retku
        if(
            this.table[0][i] !== "" // Ne smije biti sva tri prazna
            && this.table[0][i] === this.table[1][i]
            && this.table[0][i] === this.table[2][i]
        ){
          this.setWinner();
          return;
        }
      }

      // Glavna dijagonala
      if( this.table[0][0] !== ""
          && this.table[0][0] === this.table[1][1]
          && this.table[0][0] === this.table[2][2]
      ){
        this.setWinner();
        return;
      }

      // sporedna dijagonala
      if( this.table[0][2] !== ""
          && this.table[0][2] === this.table[1][1]
          && this.table[0][2] === this.table[2][0]
      ){
        this.setWinner();
      }

      // Provjera jesu li odigrani svi potezi
      let iItems = 0;
      for(i=0; i<3; i++){
        for(j=0; j<3; j++){
          if(this.table[i][j] !== "")
            iItems ++;
        }
      }
      if(iItems === 9){
        this.winner = "none";
      }
    },

    newGame:function () {
      // Inicijalizirajmo polje
      this.table =[
        ["", "", ""] ,
        ["", "", ""] ,
        ["", "", ""]
      ];

      // Sklonimo pobjednika
      this.winner = "";

      // Ovo je najmjerno zakomentirano, tako će osoba koja je "izgubila" biti prva na potezu
      // Označimo da je prvi igrač na potezu
      // this.player = true;
    }
  },
  template: '#gametable-template'
});


Vue.component('gamescores', {
  template: '#gamescores-template',
  props: [ "player1name", "player2name", "player1score",  "player2score" ]
});

var app = new Vue({
  el: '#app',
  data: function () {
    return{
      player1name: "Borna",
      player2name: "Neno",
      player1score: 0,
      player2score: 0
    }
  },
  methods:{
    increaseWinnerScore(player){
      // Povećaj broj bodova pobjedniku
      if(player === "player1"){
          this.player1score ++;
      }else{
          this.player2score ++;
      }
      this.saveValues();
    },
    saveValues(){
      // Spremi u browser trenutne bodove oba igrača
      localStorage.player1score = this.player1score;
      localStorage.player2score = this.player2score;

      localStorage.player1name = this.player1name;
      localStorage.player2name = this.player2name;
    },
    loadValues(){
      // Učitaj iz browsera (localstorage) vrijednosti igrača i bodova
      let elements = ["player1name", "player2name", "player1score", "player2score"];
      let i;
      for (i of elements) {
        if (localStorage.getItem(i) !== null) {
          this[i] = localStorage.getItem(i);
        }
      }
    },
    updatePlayerName(player){
      const newPlayerName = prompt("Upiši novo ime igrača", "");
      // Ako je upisano novo ime igrača
      if(newPlayerName !== null && newPlayerName !== ""){
        if(player === "player1"){
          this.player1name = newPlayerName;
        }else if(player === "player2"){
          this.player2name = newPlayerName;
        }
        // Spremi sve u browser
        this.saveValues();
      }
    },
    // Resetiranje bodova na nulu
    resetScore(){
      // Provjeri je li sigurno to želi
      let confirm = window.confirm("Sigurno želiš obrisati bodove na nulu?");
      if(confirm){
        // Postavi ih na nulu
        this.player1score = 0;
        this.player2score = 0;
        // Spremi u browser
        this.saveValues();
      }
    }
  },
  // Poziva se kad se kreira VueJS aplikacija
  created: function () {
    // Učitaj vrijednosti koje postoje
    this.loadValues();

    // Spremanje nema toliko smisla jer su upravo učitane vrijednosti, ali
    // ako je ovo prvi puta kad dolazimo na stranicu nismo ništa mogli učitati,
    // pa će se tako spremiti inicijalne vrijednosti
    this.saveValues();
  }
});
