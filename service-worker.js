// Što sve želimo cacheirati
const filesToCache = [
    '/',
    'styles/inline.css',
    'scripts/app.js',
    'index.html',
    'img/logo.png',
    'https://cdn.jsdelivr.net/npm/vue/dist/vue.js'
];

const staticCacheName = 'pages-cache-v1';

// Kod prvog pokretanja - dohvati sve datoteke iz gornjeg popisa u offline cache
self.addEventListener('install', function(event) {
  // console.log('[Service Worker] Installing Service Worker.....', event);

  // Dohvati sve datoteke iz popisa filesToCache i dodaj ih u lokalni cache
  event.waitUntil(
      caches.open(staticCacheName)
          .then(cache => {
            return cache.addAll(filesToCache);
          })
  );
});

self.addEventListener('activate', function(event){
  // console.log('[Service Worker] Activating Service Worker.....',event);
  return self.clients.claim();
});

// Kod svakog dohvaćanja URL-a, pokušaj dohvatiti iz cachea, ako nema onda dohvati s mreže
// i taj podatak spremi u cache
self.addEventListener('fetch', function(event){
  // console.log('[Service Worker] Fetch initiated.....',event);

  event.respondWith(
      // Ako ima u cacheu - pošalji datoteku iz cachea
      caches
          .match(event.request)
          .then(response => {
            if (response) {
              // console.log('Found ', event.request.url, ' in cache');
              return response;
            }

            // inače vrati datoteku iz mreže
            // console.log('Network request for ', event.request.url);
            return fetch(event.request)
                .then(response => {
                  // Smjesti odgovor opet u cache
                  return caches.open(staticCacheName).then(cache => {
                    cache.put(event.request.url, response.clone());
                    return response;
                  });
                });
          })
          .catch(error => {
          })
  );
});


